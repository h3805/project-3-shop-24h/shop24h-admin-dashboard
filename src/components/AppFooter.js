import React from 'react'
import { CFooter } from '@coreui/react'

const AppFooter = () => {
  return (
    <CFooter>
      <div>
        <a href="/" target="_blank">
         Devcamp120.R2112.hiennt
        </a>
        <span className="ms-1">&copy; 2022 Project.</span>
      </div>
      <div className="ms-auto">
        <span className="me-1"><b>Version </b>1.0.0</span>
      </div>
    </CFooter>
  )
}

export default React.memo(AppFooter)
