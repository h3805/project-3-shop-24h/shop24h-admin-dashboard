import { createStore } from 'redux'

const initialState = {
  sidebarShow: true,
  productCartsArr: [],
  total: 0,
}

const changeState = (state = initialState, { type, payload, ...rest }) => {
  switch (type) {
    case 'set':
      return { ...state, ...rest }
    case "ADD_CART": {
      let flag = true;
      state.productCartsArr.forEach((element, index) => {
        if (element.productId === payload.product.productId) {
          element.productQuantity = element.productQuantity + payload.product.productQuantity;
          flag = false;
        }
      });
      if (flag) {
        state.productCartsArr = [...state.productCartsArr, payload.product];
      }
      return {
        ...state,
        productCartsArr: state.productCartsArr,
        total: state.total + (payload.product.productQuantity * payload.product.productPrice),
        ...rest
      };
    }
    case "RESET_CART": {
      return {
        ...state,
        productCartsArr: [],
        total: 0,
        ...rest
      };
    }
    case "DELETE_ITEM_CART": {
      let newArr = state.productCartsArr.filter(ele => {
        return (
          ele.productId !== payload.product.productId
        )
      })
      return {
        ...state,
        productCartsArr: newArr,
        total: state.total - (payload.product.productQuantity * payload.product.productPrice),
        ...rest
      };
    }
    case "UPDATE_ITEM_CART": {
      let totalItemOld = 0;
      let totalItemNew = 0;
      state.productCartsArr.forEach((ele, index) => {
        if (ele.productId === payload.product.productId) {
          totalItemOld = state.productCartsArr[index].productQuantity * state.productCartsArr[index].productPrice;
          ele.productQuantity = payload.product.productQuantity;
        }
      })
      if (payload.product.totalAdd > 0) {
        totalItemNew = payload.product.totalAdd
      }
      return {
        ...state,
        productCartsArr: state.productCartsArr,
        total: state.total - totalItemOld + totalItemNew,
        ...rest
      };
    }
    case "SET_NEW_CART": {
      return {
        ...state,
        productCartsArr: payload.product,
        total: payload.total,
        ...rest
      };
    }
    default:
      return state
  }
}

const store = createStore(changeState)
export default store
