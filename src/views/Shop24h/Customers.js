import React, { useEffect, useState } from 'react'
import {
  CCard,
  CCardBody,
  CCardHeader,
} from '@coreui/react'
import { Box, Button, Divider, Grid, IconButton, InputBase, Modal, Pagination, Paper, Stack, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, TextField, Typography } from '@mui/material';
import { Link } from 'react-router-dom';
import { useSnackbar } from 'notistack';
import PersonAddAltIcon from '@mui/icons-material/PersonAddAlt';
import ManageAccountsIcon from '@mui/icons-material/ManageAccounts';
import UploadIcon from '@mui/icons-material/Upload';
import ContentPasteGoIcon from '@mui/icons-material/ContentPasteGo';
import PersonSearchIcon from '@mui/icons-material/PersonSearch';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: "60%",
  bgcolor: 'background.paper',
  boxShadow: 24,
  p: 2,
};

const Customers = () => {
  const { enqueueSnackbar } = useSnackbar();
  //biến lưu dữ liệu table
  const [customersData, setCustomersData] = useState([{
    address: "",
    city: "",
    country: "",
    email: "",
    fullName: "",
    phone: "",
  }])

  //khai báo fetch
  const fetchApi = async (urlApi, bodyApi) => {
    const response = await fetch(urlApi, bodyApi);
    const data = await response.json();
    return data;
  }

  //biến dùng để refresh lại table
  const [refresh, setRefresh] = useState(false);
  const getRefresh = () => {
    refresh === true ? setRefresh(false) : setRefresh(true);
  }
  //set page pagination
  const [noPage, setNoPage] = useState(0);
  const [page, setPage] = useState(1);
  const onPageChange = (event, value) => {
    setPage(value);
  }
  //Search
  const [inputEmailSearch, setInputEmailSearch] = useState("");
  //khai báo url
  const URL = "http://localhost:8000/Customers?email=" + inputEmailSearch

  //<------------------------------------------------------------------------------------- Modal Create
  //Mở, đóng modal create Customers
  const [openModalCreateCustomers, setOpenModalCreateCustomers] = useState(false);
  const handleOpenModalCreateCustomers = () => setOpenModalCreateCustomers(true);
  const handleCloseModalCreateCustomers = () => setOpenModalCreateCustomers(false);

  //khai báo biến
  const [fullNameCreateModal, setFullNameCreateModal] = useState("");
  const [phoneCreateModal, setPhoneCreateModal] = useState("");
  const [emailCreateModal, setEmailCreateModal] = useState("");
  const [addressCreateModal, setAddressCreateModal] = useState("");
  const [cityCreateModal, setCityCreateModal] = useState("");
  const [countryCreateModal, setCountryCreateModal] = useState("");

  //Hàm xử lý sự kiện khi click creat Customers trong modal
  const onSubmitCreateModal = (event) => {
    event.preventDefault();
    //1.thu thập dữ liệu
    let customersCreate = {
      fullName: fullNameCreateModal.trim(),
      phone: phoneCreateModal.trim(),
      email: emailCreateModal.trim(),
      address: addressCreateModal.trim(),
      city: cityCreateModal.trim(),
      country: countryCreateModal.trim()
    }
    //2.validate (đã validate form submit)
    //3. gọi api
    createCustomersApi(customersCreate);
  }

  //hàm tạo Customers trên api
  const createCustomersApi = (paramCustomersCreate) => {
    let body = {
      method: 'POST',
      body: JSON.stringify(paramCustomersCreate),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      }
    };
    fetchApi("http://localhost:8000/Customers", body)
      .then((response) => {
        if (response.status === 'Internal server error') {
          enqueueSnackbar('Số điện thoại / Email đã có', { variant: "error" });
          throw new Error("something wrong")
        }
        else {
          enqueueSnackbar('đã tạo Customers', { variant: "success" });
          //đóng modal
          handleCloseModalCreateCustomers();
          //reset modal
          resetCreateModal();
          //refresh table
          getRefresh();
        }
      })
      .catch((error) => {
        throw (error);
      });
  }

  //hàm reset dữ liệu create modal sau khi tạo thành công
  const resetCreateModal = () => {
    setFullNameCreateModal("");
    setPhoneCreateModal("");
    setEmailCreateModal("");
    setAddressCreateModal("");
    setCityCreateModal("");
    setCountryCreateModal("");
  }
  // end modal create handle----------------------------------------------------------------------------->

  //<------------------------------------------------------------------------------------------- Modal update
  //Mở, đóng modal update Customer
  const [openModalUpdateCustomer, setOpenModalUpdateCustomer] = useState(false);
  const handleOpenModalUpdateCustomer = () => setOpenModalUpdateCustomer(true);
  const handleCloseModalUpdateCustomer = () => setOpenModalUpdateCustomer(false);

  //khai báo biến
  const [fullNameUpdateModal, setFullNameUpdateModal] = useState("");
  const [phoneUpdateModal, setPhoneUpdateModal] = useState("");
  const [emailUpdateModal, setEmailUpdateModal] = useState("");
  const [addressUpdateModal, setAddressUpdateModal] = useState("");
  const [cityUpdateModal, setCityUpdateModal] = useState("");
  const [countryUpdateModal, setCountryUpdateModal] = useState("");
  const [ordersUpdateModal, setOrdersUpdateModal] = useState([]);
  const [timeCreatedUpdateModal, setTimeCreatedUpdateModal] = useState("");
  const [timeUpdatedUpdateModal, setTimeUpdatedUpdateModal] = useState("");
  const [customerIddUpdateModal, setCustomerIdUpdateModal] = useState("");

  //xử lý sự kiện click button Info
  const onClickInfoBtnHandle = (customersRow) => {
    setFullNameUpdateModal(customersRow.fullName);
    setPhoneUpdateModal(customersRow.phone);
    setEmailUpdateModal(customersRow.email);
    setAddressUpdateModal(customersRow.address);
    setCityUpdateModal(customersRow.city);
    setCountryUpdateModal(customersRow.country);
    setOrdersUpdateModal(customersRow.orders);
    setTimeCreatedUpdateModal(customersRow.timeCreated);
    setTimeUpdatedUpdateModal(customersRow.timeUpdated);
    setCustomerIdUpdateModal(customersRow._id);
    handleOpenModalUpdateCustomer();
  }

  //Hàm xử lý sự kiện khi click update Product trong modal
  const onSubmitUpdateModal = (event) => {
    event.preventDefault();
    //1.thu thập dữ liệu
    let customerUpdateData = {
      fullName: fullNameUpdateModal.trim(),
      phone: phoneUpdateModal.trim(),
      email: emailUpdateModal.trim(),
      address: addressUpdateModal.trim(),
      city: cityUpdateModal.trim(),
      country: countryUpdateModal.trim()
    }
    //2.validate (đẫ validate theo form submit)
    //3. gọi api
    updateCustomerApi(customerUpdateData);
  }

  //hàm update product trên api
  const updateCustomerApi = (customerUpdateData) => {
    let body = {
      method: 'PUT',
      body: JSON.stringify(customerUpdateData),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      }
    };
    fetchApi("http://localhost:8000/Customers/" + customerIddUpdateModal, body)
      .then((response) => {
        if (response.status === "Internal server error") {
          enqueueSnackbar('Số điện thoại / Email trùng với tài khoản khác', { variant: "error" });
          throw new Error("something wrong")
        }
        else {
          enqueueSnackbar('Đã cập nhật customer', { variant: "success" });
          //đóng modal
          handleCloseModalUpdateCustomer();
          //refresh table
          getRefresh();
        }
      })
      .catch((error) => {
        throw (error);
      });
  }
  //end modal update handle---------------------------------------------------------------------------------->

  useEffect(() => {
    fetchApi(URL)
      .then((data) => {
        setCustomersData(data.data);
        setNoPage(Math.ceil(data.data.length / 5));
      })
      .catch((error) => {
        throw (error);
      });
  }, [refresh, URL])

  return (
    <div>
      <CCard className="mb-4">
        <CCardHeader>
          <h3>Customers Table</h3>
        </CCardHeader>
        <CCardBody>
          <Stack spacing={2}>
            <div>
              <Grid container justifyContent="space-between" alignItems="center">
                <Grid item>
                  <Button color="success" variant='contained' startIcon={<PersonAddAltIcon />} className='float-end' onClick={handleOpenModalCreateCustomers}>Create</Button>
                </Grid>
                <Grid item>
                  <Paper
                    component="form"
                    sx={{ display: 'flex', alignItems: 'center', paddingX: 2 }}
                  >
                    <InputBase placeholder="Search Customer Email....." value={inputEmailSearch} onChange={(event) => setInputEmailSearch(event.target.value)} />
                    <IconButton type="submit" sx={{ p: '10px' }} aria-label="search" disabled>
                      <PersonSearchIcon />
                    </IconButton>
                  </Paper>
                </Grid>
              </Grid>
            </div>
            <TableContainer component={Paper}>
              <Table sx={{ minWidth: 650 }} aria-label="cart table">
                <TableHead>
                  <TableRow className='bg-secondary bg-gradient'>
                    <TableCell align="center">#</TableCell>
                    <TableCell align="center">Full Name</TableCell>
                    <TableCell align="center">Email</TableCell>
                    <TableCell align="center">Phone</TableCell>
                    <TableCell align="center">Address</TableCell>
                    <TableCell align="center">Action</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {
                    customersData
                      .slice(5 * (page - 1), 5 * page)
                      .map((ele, index) => {
                        return (
                          <TableRow key={index}>
                            <TableCell align="center">{index + 1}</TableCell>
                            <TableCell align="center">{ele.fullName}</TableCell>
                            <TableCell align="center">{ele.email}</TableCell>
                            <TableCell align="center">{ele.phone}</TableCell>
                            <TableCell align="left" >{ele.address}, {ele.city}, {ele.country}</TableCell>
                            <TableCell align="center">
                              <Button variant='contained' color='info' startIcon={<ManageAccountsIcon />} size='small' onClick={() => onClickInfoBtnHandle(ele)}>Info</Button>
                            </TableCell>
                          </TableRow>
                        )
                      })
                  }
                </TableBody>
              </Table>
            </TableContainer>
            <Grid sx={{ display: "flex", justifyContent: "flex-end" }}>
              <Pagination showFirstButton showLastButton count={noPage} page={page} onChange={onPageChange} />
            </Grid>
          </Stack>
        </CCardBody>
      </CCard>

      {/* Modal Creat Customers */}
      <Modal open={openModalCreateCustomers}>
        <Box sx={style}>
          <form onSubmit={onSubmitCreateModal}>
            <Stack spacing={2}>
              <h3 className='text-center'>CREAT CUSTOMER</h3>
              <Divider />
              <TextField required label="Full Name" fullWidth variant="standard" value={fullNameCreateModal} onChange={(event) => setFullNameCreateModal(event.target.value)} />
              <TextField required type="email" label="Email" fullWidth variant="standard" value={emailCreateModal} onChange={(event) => setEmailCreateModal(event.target.value)} />
              <TextField required type="number" label="Phone number" fullWidth variant="standard" value={phoneCreateModal} onChange={(event) => setPhoneCreateModal(event.target.value)} />
              <TextField label="Address" fullWidth variant="standard" value={addressCreateModal} onChange={(event) => setAddressCreateModal(event.target.value)} />
              <Stack direction="row" spacing={2}>
                <TextField label="City" fullWidth variant="standard" value={cityCreateModal} onChange={(event) => setCityCreateModal(event.target.value)} />
                <TextField label="Country" fullWidth variant="standard" value={countryCreateModal} onChange={(event) => setCountryCreateModal(event.target.value)} />
              </Stack>
              <Stack direction="row" spacing={2} justifyContent="space-between">
                <Button type="submit" variant="contained" color="success" startIcon={<PersonAddAltIcon />} > Create</Button>
                <Button variant="outlined" onClick={handleCloseModalCreateCustomers}>Cancel</Button>
              </Stack>
            </Stack>
          </form>
        </Box>
      </Modal >

      {/* Modal Update Customers */}
      <Modal open={openModalUpdateCustomer} onClose={handleCloseModalUpdateCustomer}>
        <Box sx={style} style={{ maxHeight: "85%", overflow: "auto" }}>
          <form onSubmit={onSubmitUpdateModal}>
            <Grid container spacing={2}>
              <Grid item xs={12} md={7}>
                <Stack spacing={2}>
                  <h4>CUSTOMER DETAILS</h4>
                  <TextField required label="Full Name" fullWidth variant="standard" value={fullNameUpdateModal} onChange={(event) => { setFullNameUpdateModal(event.target.value) }} />
                  <TextField required type="email" label="Email" fullWidth variant="standard" value={emailUpdateModal} onChange={(event) => { setEmailUpdateModal(event.target.value) }} />
                  <TextField required type="number" label="Phone number" fullWidth variant="standard" value={phoneUpdateModal} onChange={(event) => { setPhoneUpdateModal(event.target.value) }} />
                  <TextField label="Address" fullWidth variant="standard" value={addressUpdateModal} onChange={(event) => { setAddressUpdateModal(event.target.value) }} />
                  <Stack direction="row" spacing={2}>
                    <TextField label="City" fullWidth variant="standard" value={cityUpdateModal} onChange={(event) => { setCityUpdateModal(event.target.value) }} />
                    <TextField label="Country" fullWidth variant="standard" value={countryUpdateModal} onChange={(event) => { setCountryUpdateModal(event.target.value) }} />
                  </Stack>
                  <Stack direction="row" spacing={2}>
                    <TextField label="Time Created" fullWidth variant="standard" value={timeCreatedUpdateModal} disabled />
                    <TextField label="Time Updated" fullWidth variant="standard" value={timeUpdatedUpdateModal} disabled />
                  </Stack>
                  <Stack direction="row" spacing={2} justifyContent="space-between">
                    <Button type="submit" variant="contained" color="info" startIcon={<UploadIcon />}>Update</Button>
                    <Button variant="outlined" onClick={handleCloseModalUpdateCustomer}>Cancel</Button>
                  </Stack>
                </Stack>
              </Grid>
              <Grid item xs={12} md={5}>
                <h4>CUSTOMER ORDERS</h4>
                <Grid container className='p-3 mb-3 bg-light rounded-3' sx={{ maxHeight: 380, overflow: "auto" }}>
                  <Grid item xs={12}>
                    <Stack spacing={1}>
                      {
                        ordersUpdateModal.length > 0 ?
                          ordersUpdateModal.map((ele, index) => {
                            return (
                              <Typography variant='body2' key={index}>Order Id: {ele}</Typography>
                            )
                          })
                          :
                          <Typography variant='body2'>No Order</Typography>
                      }
                    </Stack>
                  </Grid>
                </Grid>
                <Grid>
                  <Link to={"/Customers/" + customerIddUpdateModal} style={{ color: "inherit", textDecorationLine: "none" }}>
                    {
                      ordersUpdateModal.length > 0 ?
                        <Button variant="contained" fullWidth color="warning" size='large' startIcon={<ContentPasteGoIcon />}>Go to view detail...</Button>
                        :
                        <Button variant="contained" fullWidth color="success" size='large' startIcon={<ContentPasteGoIcon />}>Go to create ...</Button>
                    }
                  </Link>
                </Grid>
              </Grid>
            </Grid>
          </form>
        </Box>
      </Modal>
    </div >
  )
}

export default Customers
