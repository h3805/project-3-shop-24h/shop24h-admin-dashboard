import React from 'react'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CLink,
  CRow,
  CWidgetStatsF,
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import {
  cilArrowRight,
  cilPeople,
  cilNotes,
  cilTags,
  cilDevices,
} from '@coreui/icons'

const Shop24hHome = () => {
  return (
    <div>
      <CCard className="mb-4">
        <CCardHeader>
          <h3>Shop 24h Home</h3>
        </CCardHeader>
        <CCardBody>
          <CRow>
            <CCol xs={12} sm={6} lg={3}>
              <CWidgetStatsF
                className="mb-3"
                icon={<CIcon width={24} icon={cilPeople} size="xl" />}
                value="Customers"
                color="primary"
                footer={
                  <CLink
                    className="font-weight-bold font-xs text-medium-emphasis"
                    href='/#/Customers'
                  >
                    Go
                    <CIcon icon={cilArrowRight} className="float-end" width={16} />
                  </CLink>
                }
              />
            </CCol>
            <CCol xs={12} sm={6} lg={3}>
              <CWidgetStatsF
                className="mb-3"
                icon={<CIcon width={24} icon={cilNotes} size="xl" />}
                value="Orders"
                color="info"
                footer={
                  <CLink
                    className="font-weight-bold font-xs text-medium-emphasis"
                    href='/#/Orders'
                  >
                    Go
                    <CIcon icon={cilArrowRight} className="float-end" width={16} />
                  </CLink>
                }
              />
            </CCol>
            <CCol xs={12} sm={6} lg={3}>
              <CWidgetStatsF
                className="mb-3"
                icon={<CIcon width={24} icon={cilDevices} size="xl" />}
                value="Products"
                color="warning"
                footer={
                  <CLink
                    className="font-weight-bold font-xs text-medium-emphasis"
                    href='/#/Products'
                  >
                    Go
                    <CIcon icon={cilArrowRight} className="float-end" width={16} />
                  </CLink>
                }
              />
            </CCol>
            <CCol xs={12} sm={6} lg={3}>
              <CWidgetStatsF
                className="mb-3"
                icon={<CIcon width={24} icon={cilTags} size="xl" />}
                value="Product Types"
                color="danger"
                footer={
                  <CLink
                    className="font-weight-bold font-xs text-medium-emphasis"
                    href='/#/ProductTypes'
                  >
                    Go
                    <CIcon icon={cilArrowRight} className="float-end" width={16} />
                  </CLink>
                }
              />
            </CCol>
          </CRow>
        </CCardBody>
      </CCard>
    </div>
  )
}

export default Shop24hHome
