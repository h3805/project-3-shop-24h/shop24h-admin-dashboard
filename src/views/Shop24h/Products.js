import React, { useEffect, useState } from 'react'
import {
  CCard,
  CCardBody,
  CCardHeader
} from '@coreui/react'
import { Box, Button, Divider, Grid, MenuItem, Modal, Pagination, Paper, Select, Stack, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, TextareaAutosize, TextField, Typography, InputBase, IconButton } from '@mui/material';
import { useSnackbar } from 'notistack';
import AddToQueueIcon from '@mui/icons-material/AddToQueue';
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';
import DeleteIcon from '@mui/icons-material/Delete';
import UploadIcon from '@mui/icons-material/Upload';
import ScreenSearchDesktopIcon from '@mui/icons-material/ScreenSearchDesktop';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: "60%",
  bgcolor: 'background.paper',
  boxShadow: 24,
  p: 2,
};

const Products = () => {
  const { enqueueSnackbar } = useSnackbar();
  //khai báo fetch
  const fetchApi = async (urlApi, bodyApi) => {
    const response = await fetch(urlApi, bodyApi);
    const data = await response.json();
    return data;
  }

  //biến dataTypes lưu thông tin types để load vào select và render lại type name
  const [dataTypes, setDataTypes] = useState([]);
  //biến lưu dữ liệu table
  const [ProductsData, setProductsData] = useState([{
    buyPrice: 0,
    description: "",
    imageUrl: "",
    name: "",
    promotionPrice: 0,
    type: "",
    typeName: "",
  }])

  //biến dùng để refresh lại table
  const [refresh, setRefresh] = useState(false);
  const getRefresh = () => {
    refresh === true ? setRefresh(false) : setRefresh(true);
  }

  //set page pagination
  const [noPage, setNoPage] = useState(0);
  const [page, setPage] = useState(1);
  const onPageChange = (event, value) => {
    setPage(value);
  }
  //Search
  const [inputNameSearch, setInputNameSearch] = useState("");
  //khai báo url
  const URL = "http://localhost:8000/Products?name=" + inputNameSearch

  //<------------------------------------------------------------------------------------- Modal Create
  //Mở, đóng modal create Product
  const [openModalCreateProduct, setOpenModalCreateProduct] = useState(false);
  const handleOpenModalCreateProduct = () => setOpenModalCreateProduct(true);
  const handleCloseModalCreateProduct = () => setOpenModalCreateProduct(false);

  //khai báo biến
  const [nameCreateModal, setNameCreateModal] = useState("");
  const [descriptionCreateModal, setDescriptionCreateModal] = useState("");
  const [typeCreateModal, setTypeIdCreateModal] = useState("none");
  const [imageUrlCreateModal, setImageUrlCreateModal] = useState("");
  const [buyPriceCreateModal, setBuyPriceCreateModal] = useState(0);
  const [promotionPriceCreateModal, setPromotionPriceCreateModal] = useState(0);
  const [amountCreateModal, setAmountCreateModal] = useState(0);

  //Hàm xử lý sự kiện khi click creat Product trong modal
  const onSubmitCreateModal = (event) => {
    event.preventDefault();
    //1.thu thập dữ liệu
    let productSendData = {
      name: nameCreateModal.trim(),
      description: descriptionCreateModal.trim(),
      type: typeCreateModal,
      imageUrl: imageUrlCreateModal.trim(),
      buyPrice: parseInt(buyPriceCreateModal),
      promotionPrice: parseInt(promotionPriceCreateModal),
      amount: parseInt(amountCreateModal)
    }
    //2.validate
    let isValidated = getValidated(productSendData);
    if (isValidated) {
      //3. gọi api
      createProductApi(productSendData);
    }
  }

  //hàm validate dữ liệu 
  const getValidated = (paramProductSendData) => {
    if (paramProductSendData.type === "none") {
      enqueueSnackbar('Please select type!', { variant: "error" });
      return false;
    }
    if (paramProductSendData.promotionPrice <= 0) {
      enqueueSnackbar('Promotion Price must > 0', { variant: "error" });
      return false;
    }
    if (paramProductSendData.promotionPrice > paramProductSendData.buyPrice) {
      enqueueSnackbar('Buy Price must > Promotion Price', { variant: "error" });
      return false;
    }
    if (paramProductSendData.amount < 0) {
      enqueueSnackbar('Amount must >= 0', { variant: "error" });
      return false;
    }
    return true;
  }

  const setAmountInput = (value) => {
    if (value < 0) setAmountUpdateModal(0)
    else setAmountUpdateModal(value);
  }
  //hàm tạo product trên api
  const createProductApi = (productSendData) => {
    let body = {
      method: 'POST',
      body: JSON.stringify(productSendData),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      }
    };
    fetchApi("http://localhost:8000/Products", body)
      .then((response) => {
        if (response.status === 'Internal server error') {
          if (response.messager.includes("name_1 dup key")) {
            enqueueSnackbar('tên sản phẩm đã có', { variant: "error" });
          }
          else throw new Error("something wrong")
        }
        else {
          enqueueSnackbar('đã tạo product', { variant: "success" });
          //đóng modal
          handleCloseModalCreateProduct();
          //reset modal
          resetCreateModal();
          //refresh table
          getRefresh();
        }
      })
      .catch((error) => {
        throw (error);
      });
  }

  //hàm reset dữ liệu create modal sau khi tạo thành công
  const resetCreateModal = () => {
    setNameCreateModal("");
    setDescriptionCreateModal("");
    setTypeIdCreateModal("none");
    setImageUrlCreateModal("");
    setBuyPriceCreateModal(0);
    setPromotionPriceCreateModal(0);
    setAmountCreateModal(0);
  }
  // end modal create handle----------------------------------------------------------------------------->

  //<------------------------------------------------------------------------------------------- Modal update
  //Mở, đóng modal update Product
  const [openModalUpdateProduct, setOpenModalUpdateProduct] = useState(false);
  const handleOpenModalUpdateProduct = () => setOpenModalUpdateProduct(true);
  const handleCloseModalUpdateProduct = () => setOpenModalUpdateProduct(false);

  //khai báo biến
  const [nameUpdateModal, setNameUpdateModal] = useState("");
  const [descriptionUpdateModal, setDescriptionUpdateModal] = useState("");
  const [typeUpdateModal, setTypeIdUpdateModal] = useState("none");
  const [imageUrlUpdateModal, setImageUrlUpdateModal] = useState("");
  const [buyPriceUpdateModal, setBuyPriceUpdateModal] = useState(0);
  const [promotionPriceUpdateModal, setPromotionPriceUpdateModal] = useState(0);
  const [amountUpdateModal, setAmountUpdateModal] = useState(0);
  const [productIdUpdateModal, setProductIdUpdateModal] = useState(0);
  const [timeCreatedUpdateModal, setTimeCreatedUpdateModal] = useState(0);
  const [timeUpdatedUpdateModal, setTimeUpdatedUpdateModal] = useState(0);

  //xử lý sự kiện click button Info
  const onClickInfoBtnHandle = (productRow) => {
    setNameUpdateModal(productRow.name);
    setDescriptionUpdateModal(productRow.description);
    setTypeIdUpdateModal(productRow.typeName);
    setImageUrlUpdateModal(productRow.imageUrl);
    setBuyPriceUpdateModal(productRow.buyPrice);
    setPromotionPriceUpdateModal(productRow.promotionPrice);
    setAmountUpdateModal(productRow.amount);
    setProductIdUpdateModal(productRow._id);
    setTimeCreatedUpdateModal(productRow.timeCreated);
    setTimeUpdatedUpdateModal(productRow.timeUpdated);
    handleOpenModalUpdateProduct();
  }

  //Hàm xử lý sự kiện khi click update Product trong modal
  const onSubmitUpdateModal = (event) => {
    event.preventDefault();
    //1.thu thập dữ liệu
    let productSendData = {
      name: nameUpdateModal.trim(),
      description: descriptionUpdateModal.trim(),
      type: typeUpdateModal,
      imageUrl: imageUrlUpdateModal.trim(),
      buyPrice: parseInt(buyPriceUpdateModal),
      promotionPrice: parseInt(promotionPriceUpdateModal),
      amount: amountUpdateModal ? parseInt(amountUpdateModal) : 0
    }
    //2.validate
    let isValidated = getValidated(productSendData);
    if (isValidated) {
      //3. gọi api
      updateProductApi(productSendData);
    }
  }

  //hàm update product trên api
  const updateProductApi = (productSendData) => {
    let body = {
      method: 'PUT',
      body: JSON.stringify(productSendData),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      }
    };
    fetchApi("http://localhost:8000/Products/" + productIdUpdateModal, body)
      .then((response) => {
        if (response.status === "Internal server error") {
          enqueueSnackbar('tên sản phẩm đã có', { variant: "error" });
          throw new Error("something wrong")
        }
        else {
          enqueueSnackbar('đã cập nhật product', { variant: "success" });
          //đóng modal
          handleCloseModalUpdateProduct();
          //refresh table
          getRefresh();
        }
      })
      .catch((error) => {
        throw (error);
      });
  }
  //end modal update handle---------------------------------------------------------------------------------->

  //<------------------------------------------------------------------------------------------- Modal delete
  //Mở, đóng modal Delete Product
  const [openModalDeleteProduct, setOpenModalDeleteProduct] = useState(false);
  const handleOpenModalDeleteProduct = () => setOpenModalDeleteProduct(true);
  const handleCloseModalDeleteProduct = () => setOpenModalDeleteProduct(false);
  const [productIdDeleteModal, setProductIdDeleteModal] = useState(0);

  //xử lý sự kiện click button delete
  const onClickDeleteBtnHandle = (productRow) => {
    setProductIdDeleteModal(productRow._id);
    handleOpenModalDeleteProduct();
  }

  //Hàm xử lý sự kiện khi click confirm delete Product
  const onSubmitDeleteModal = (event) => {
    event.preventDefault();
    let id = productIdDeleteModal;
    let body = {
      method: 'DELETE'
    };
    fetch("http://localhost:8000/Products/" + id, body)
      .then(() => {
        enqueueSnackbar('Đã Xóa sản phẩm thành công!', { variant: "success" });
        //đóng modal
        handleCloseModalDeleteProduct();
        //refresh table
        getRefresh();
      })
      .catch((error) => {
        throw (error);
      })
  }
  //end modal delete handle---------------------------------------------------------------------------------->

  useEffect(() => {
    fetchApi(URL)
      .then((productsData) => {
        setNoPage(Math.ceil(productsData.data.length / 5));
        fetchApi("http://localhost:8000/ProductTypes/")
          .then((typeData) => {
            setDataTypes(typeData.data);
            let arrayElement = {};
            let arrayResult = [];
            productsData.data.forEach(eleProducts => {
              typeData.data.forEach(eleType => {
                if (eleProducts.type === eleType._id) {
                  arrayElement = eleProducts;
                  arrayElement.typeName = eleType.name;
                  arrayResult.push(arrayElement);
                }
              });
            });
            setProductsData(arrayResult);
          })
          .catch((error) => {
            throw (error);
          });
      })
      .catch((error) => {
        throw (error);
      });
  }, [refresh, URL])
  return (
    <div>
      <CCard className="mb-4">
        <CCardHeader>
          <h3>Products Table</h3>
        </CCardHeader>
        <CCardBody>
          <Stack spacing={2}>
            <div>
              <Grid container justifyContent="space-between" alignItems="center">
                <Grid item>
                  <Button variant='contained' color='success' endIcon={<AddToQueueIcon />} size='large' className='float-end' onClick={handleOpenModalCreateProduct}>Create</Button>
                </Grid>
                <Grid item>
                  <Paper
                    component="form"
                    sx={{ display: 'flex', alignItems: 'center', paddingX: 2 }}
                  >
                    <InputBase placeholder="Search Product Name....." value={inputNameSearch} onChange={(event) => setInputNameSearch(event.target.value)} />
                    <IconButton type="submit" sx={{ p: '10px' }} aria-label="search" disabled>
                      <ScreenSearchDesktopIcon />
                    </IconButton>
                  </Paper>
                </Grid>
              </Grid>
            </div>
            <TableContainer component={Paper}>
              <Table sx={{ minWidth: 650 }} aria-label="cart table">
                <TableHead>
                  <TableRow className='bg-secondary bg-gradient'>
                    <TableCell align="center">#</TableCell>
                    <TableCell align="center">Name</TableCell>
                    <TableCell align="center">Buy Price</TableCell>
                    <TableCell align="center">Promotion Price</TableCell>
                    <TableCell align="center">Description</TableCell>
                    <TableCell align="center">Categories</TableCell>
                    <TableCell align="center">Action</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {
                    ProductsData
                      .slice(5 * (page - 1), 5 * page)
                      .map((ele, index) => {
                        return (
                          <TableRow key={index}>
                            <TableCell align="center">{index + 1}</TableCell>
                            <TableCell align="center" sx={{ maxWidth: "100px" }}>
                              <Typography noWrap>{ele.name}
                              </Typography>
                            </TableCell>
                            <TableCell align="center">{ele.buyPrice.toLocaleString('vi-VN', { style: 'currency', currency: 'VND' })}</TableCell>
                            <TableCell align="center">{ele.promotionPrice.toLocaleString('vi-VN', { style: 'currency', currency: 'VND' })}</TableCell>
                            <TableCell align="left" sx={{ maxWidth: "100px" }}>
                              {/* {ele.description} */}
                              <Typography noWrap>
                                {ele.description}
                              </Typography>
                            </TableCell>
                            <TableCell align="center">{ele.typeName}</TableCell>
                            <TableCell align="center">
                              <Typography noWrap>
                                <Button variant='contained' color='info' startIcon={<InfoOutlinedIcon />} size='small' onClick={() => onClickInfoBtnHandle(ele)}>Info</Button>&nbsp;
                                <Button variant='contained' color='error' startIcon={<DeleteIcon />} size='small' onClick={() => onClickDeleteBtnHandle(ele)}>Del</Button>
                              </Typography>
                            </TableCell>
                          </TableRow>
                        )
                      })
                  }
                </TableBody>
              </Table>
            </TableContainer>
            <Grid sx={{ display: "flex", justifyContent: "flex-end" }}>
              <Pagination
                showFirstButton
                showLastButton
                count={noPage}
                page={page}
                onChange={onPageChange}
              />
            </Grid>
          </Stack>
        </CCardBody>
      </CCard>

      {/* Modal Creat Product */}
      <Modal open={openModalCreateProduct} id="modal-insert-product" aria-labelledby="modal-modal-title" aria-describedby="modal-modal-description" >
        <Box sx={style} style={{ maxHeight: "85%", overflow: "auto" }}>
          <Typography id="modal-modal-title" variant="h4" component="h2" textAlign="center" marginBottom={2}>
            Create Product
          </Typography>
          <Divider />
          <form onSubmit={onSubmitCreateModal}>
            <Grid container spacing={2} marginTop={3} justifyContent="space-around">
              <Grid item xs={12} sm={12} md={4}>
                <Stack spacing={2}>
                  <label>Product Name<span className='text-danger'>*</span></label>
                  <TextField required variant="outlined" value={nameCreateModal} fullWidth onChange={(event) => { setNameCreateModal(event.target.value) }} />
                </Stack>
              </Grid>
              <Grid item xs={12} sm={12} md={4}>
                <Stack spacing={2}>
                  <label>Categories<span className='text-danger'>*</span></label>
                  <Select
                    value={typeCreateModal}
                    onChange={(event) => { setTypeIdCreateModal(event.target.value) }}
                    displayEmpty
                    inputProps={{ 'aria-label': 'Without label' }}
                    fullWidth
                  >
                    <MenuItem value="none">Select Type</MenuItem>
                    {
                      dataTypes.length > 0 ?
                        dataTypes.map((ele, index) => {
                          return (
                            <MenuItem key={index} value={ele.name}>{ele.name}</MenuItem>
                          )
                        })
                        :
                        <MenuItem></MenuItem>
                    }
                  </Select>
                </Stack>
              </Grid>
              <Grid item xs={12} sm={12} md={4}>
                <Stack spacing={2}>
                  <label>Image File<span className='text-danger'>*</span><small><em className='text-muted'>(VD: logo.jpg)</em></small></label>
                  <TextField required variant="outlined" value={imageUrlCreateModal} fullWidth onChange={(event) => { setImageUrlCreateModal(event.target.value) }} />
                </Stack>
              </Grid>
              <Grid item xs={12} sm={12} md={4}>
                <Stack spacing={2}>
                  <label>Buy Price<span className='text-danger'>*</span></label>
                  <TextField required type="number" variant="outlined" value={buyPriceCreateModal} fullWidth onChange={(event) => { setBuyPriceCreateModal(event.target.value) }} />
                </Stack>
              </Grid>
              <Grid item xs={12} sm={12} md={4}>
                <Stack spacing={2}>
                  <label>Promotion Price<span className='text-danger'>*</span></label>
                  <TextField required type="number" variant="outlined" value={promotionPriceCreateModal} fullWidth onChange={(event) => { setPromotionPriceCreateModal(event.target.value) }} />
                </Stack>
              </Grid>
              <Grid item xs={12} sm={12} md={4}>
                <Stack spacing={2}>
                  <label>Amount</label>
                  <TextField type="number" variant="outlined" value={amountCreateModal} fullWidth onChange={(event) => { setAmountCreateModal(event.target.value) }} />
                </Stack>
              </Grid>
              <Grid item xs={12} sm={12} md={12}>
                <Stack spacing={2}>
                  <label>Description</label>
                  <TextareaAutosize
                    minRows={3}
                    style={{ width: "100%" }}
                    value={descriptionCreateModal}
                    onChange={(event) => { setDescriptionCreateModal(event.target.value) }}
                  />
                </Stack>
              </Grid>
            </Grid>
            <Grid container spacing={2} marginTop={3} justifyContent="space-around">
              <Button type="submit" variant="contained" color="success" startIcon={<AddToQueueIcon />}>Create</Button>
              <Button variant="outlined" onClick={handleCloseModalCreateProduct}>Cancel</Button>
            </Grid>
          </form>
        </Box>
      </Modal >

      {/* Modal Update Product */}
      <Modal
        open={openModalUpdateProduct}
        onClose={handleCloseModalUpdateProduct}
        id="modal-update-product"
        aria-labelledby="modal-modal-title" aria-describedby="modal-modal-description"

      >
        <Box sx={style} style={{ maxHeight: "85%", overflow: "auto" }}>
          <Typography id="modal-modal-title" variant="h4" component="h2" textAlign="center" marginBottom={2}>
            Update Product
          </Typography>
          <Divider />
          <form onSubmit={onSubmitUpdateModal}>
            <Grid container spacing={2} marginTop={3} justifyContent="space-around">
              <Grid item xs={12} sm={12} md={12}>
                <Stack spacing={2}>
                  <label>Product ID</label>
                  <TextField required variant="outlined" value={productIdUpdateModal} fullWidth disabled />
                </Stack>
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <Stack spacing={2}>
                  <label>Time Created</label>
                  <TextField required variant="outlined" value={timeCreatedUpdateModal} fullWidth disabled />
                </Stack>
              </Grid>
              <Grid item xs={12} sm={12} md={6}>
                <Stack spacing={2}>
                  <label>Time Updated</label>
                  <TextField required variant="outlined" value={timeUpdatedUpdateModal} fullWidth disabled />
                </Stack>
              </Grid>
              <Grid item xs={12} sm={12} md={4}>
                <Stack spacing={2}>
                  <label>Product Name<span className='text-danger'>*</span></label>
                  <TextField required variant="outlined" value={nameUpdateModal} fullWidth onChange={(event) => { setNameUpdateModal(event.target.value) }} />
                </Stack>
              </Grid>
              <Grid item xs={12} sm={12} md={4}>
                <Stack spacing={2}>
                  <label>Categories<span className='text-danger'>*</span></label>
                  <Select
                    value={typeUpdateModal}
                    onChange={(event) => { setTypeIdUpdateModal(event.target.value) }}
                    displayEmpty
                    inputProps={{ 'aria-label': 'Without label' }}
                    fullWidth
                  >
                    <MenuItem value="none">Select Type</MenuItem>
                    {
                      dataTypes.length > 0 ?
                        dataTypes.map((ele, index) => {
                          return (
                            <MenuItem key={index} value={ele.name}>{ele.name}</MenuItem>
                          )
                        })
                        :
                        <MenuItem></MenuItem>
                    }
                  </Select>
                </Stack>
              </Grid>
              <Grid item xs={12} sm={12} md={4}>
                <Stack spacing={2}>
                  <label>Image File<span className='text-danger'>*</span><small><em className='text-muted'>(VD: logo.jpg)</em></small></label>
                  <TextField required variant="outlined" value={imageUrlUpdateModal} fullWidth onChange={(event) => { setImageUrlUpdateModal(event.target.value) }} />
                </Stack>
              </Grid>
              <Grid item xs={12} sm={12} md={4}>
                <Stack spacing={2}>
                  <label>Buy Price<span className='text-danger'>*</span></label>
                  <TextField required type="number" variant="outlined" value={buyPriceUpdateModal} fullWidth onChange={(event) => { setBuyPriceUpdateModal(event.target.value) }} />
                </Stack>
              </Grid>
              <Grid item xs={12} sm={12} md={4}>
                <Stack spacing={2}>
                  <label>Promotion Price<span className='text-danger'>*</span></label>
                  <TextField required type="number" variant="outlined" value={promotionPriceUpdateModal} fullWidth onChange={(event) => { setPromotionPriceUpdateModal(event.target.value) }} />
                </Stack>
              </Grid>
              <Grid item xs={12} sm={12} md={4}>
                <Stack spacing={2}>
                  <label>Amount</label>
                  <TextField type="number" variant="outlined" value={amountUpdateModal} fullWidth onChange={(event) => { setAmountInput(event.target.value) }} />
                </Stack>
              </Grid>
              <Grid item xs={12} sm={12} md={12}>
                <Stack spacing={2}>
                  <label>Description</label>
                  <TextareaAutosize
                    minRows={3}
                    style={{ width: "100%" }}
                    value={descriptionUpdateModal}
                    onChange={(event) => { setDescriptionUpdateModal(event.target.value) }}
                  />
                </Stack>
              </Grid>
            </Grid>
            <Grid container spacing={2} marginTop={3} justifyContent="space-around">
              <Button type="submit" variant="contained" color="info" startIcon={<UploadIcon />}>Update</Button>
              <Button variant="outlined" onClick={handleCloseModalUpdateProduct}>Cancel</Button>
            </Grid>
          </form>
        </Box>
      </Modal >

      {/* Modal delete product */}
      <Modal open={openModalDeleteProduct} onClose={handleCloseModalDeleteProduct} id="modal-delete-product" aria-labelledby="modal-modal-title" aria-describedby="modal-modal-description" >
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h4" component="h2" textAlign="center" marginBottom={2}>
            Confirm xóa sản phẩm!
          </Typography>
          <Divider />
          <form onSubmit={onSubmitDeleteModal}>
            <Grid container spacing={1} marginTop={3} marginBottom={3}>
              <Grid item xs={12}>
                <Typography variant="h6">Bạn có chắc chắn muốn xóa sản phẩm này không?</Typography>
              </Grid>
            </Grid>
            <Divider />
            <Grid container spacing={2} marginTop={3} justifyContent="space-around">
              <Button type="submit" variant="contained" color="error" startIcon={<DeleteIcon />}>Confirm</Button>
              <Button variant="outlined" onClick={handleCloseModalDeleteProduct}>Cancel</Button>
            </Grid>
          </form>
        </Box>
      </Modal>
    </div >
  )
}

export default Products
